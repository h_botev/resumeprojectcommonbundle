<?php
namespace Docs\CommonBundle\Utils;

class Utils
{

    /**
     * Check if variable is unsigned int
     *
     * @param mixed $value
     * @return boolean
     */
    public static function isUInt($value)
    {
        if (!is_string($value) && !is_numeric($value)) {
            return false;
        }

        return preg_match('/^[0-9]+$/i', $value);
    }

    /**
     * Checks if string is empty
     *
     * @param mixed $value
     * @return boolean
     */
    public static function isEmptyString($value)
    {
        if (!is_string($value) && !is_numeric($value)) {
            return false;
        }

        return (mb_strlen(trim($value))) ? false : true;
    }
}
