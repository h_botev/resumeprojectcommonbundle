<?php

namespace Docs\CommonBundle\Doctrine\Mapping;


/**
 * Interface for validators of entity associations mappings
 */
interface ValidatorInterface
{

    /**
     * Provides validation logic on provided mapping
     *
     * @param array $mapping
     * @return boolean
     * @throws \Exception
     */
    public function validateMapping($mapping);
}
