<?php

namespace Docs\CommonBundle\Doctrine\Mapping;

/**
 * this class was ported from ewt-common
 */

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;
use Docs\CommonBundle\Doctrine\Repository\Exception;
use Docs\CommonBundle\Doctrine\Mapping\Schema;

/**
 * Validator implementation for Mapping Schema validation
 */
class Validator implements ValidatorInterface
{

    /**
     * @inheritDoc
     */
    public function validateMapping($mapping)
    {
        if (!is_array($mapping) || empty($mapping)) {
            throw new Exception("Entities relations map should be not empty array");
        }

        if (!array_key_exists(AbstractRepository::OPERATION_READ, $mapping)) {
            throw new Exception(
                "Entities relations map should have mandatory key for operation `"
                . AbstractRepository::OPEARATION_READ . "`"
            );
        }
        $queue = new \SplQueue();

        $queue->enqueue($mapping[AbstractRepository::OPERATION_READ]);

        if (array_key_exists(AbstractRepository::OPERATION_SAVE, $mapping)) {
            $queue->enqueue($mapping[AbstractRepository::OPERATION_SAVE]);
        }

        while (!$queue->isEmpty() && $testArray = $queue->dequeue()) {

            $entity = current($testArray);

            if (!array_key_exists(Schema::$entityKey, $entity)) {
                throw new Exception(
                    "When defining association key `"
                    . Schema::$entityKey . "` is mandatory for defining concrete namespaced class"
                );
            }

            if (array_key_exists(Schema::$associationsKey, $entity)) {
                if (!is_array($entity[Schema::$associationsKey]) || empty($entity[Schema::$associationsKey])) {
                    throw new Exception('Associations for entity should be not empty array');
                }

                foreach ($entity[Schema::$associationsKey] as $key => $association) {
                    $queue->enqueue(array($key => $entity[Schema::$associationsKey][$key]));
                }
            }


            if (!array_key_exists(Schema::$aliasKey, $entity)) {
                continue;
            }

            if (Utils::isEmptyString($entity[Schema::$aliasKey])) {
                throw new Exception("The alias key `" . Schema::$aliasKey . "` of entity should be not empty string");
            }
        }

        return true;
    }
}
