<?php
namespace Docs\CommonBundle\Doctrine\Mapping;

/**
 * Mapping Schema for association entities
 * This class was ported from ewt library
 */
class Schema
{

    /**
     * The entity key
     *
     * @var string
     */
    public static $entityKey = 'entity';

    /**
     * The associations key
     *
     * @var string
     */
    public static $associationsKey = 'associations';

    /**
     * The required key
     *
     * @var string
     */
    public static $requiredKey = 'required';

    /**
     * The alias key
     *
     * @var string
     */
    public static $aliasKey = 'alias';

    /**
     * The isToMany key
     *
     * @var string
     */
    public static $isToManyKey = 'isToMany';
}
