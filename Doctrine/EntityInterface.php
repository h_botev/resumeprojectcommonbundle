<?php
namespace Docs\CommonBundle\Doctrine;

/**
 * Interface implemented by the docs entities
 * @author hbotev
 *
 */
interface EntityInterface
{
    /**
     * Populate entity from array
     * @param array $data
     * @return \Docs\CommonBundle\Doctrine\EntityInterface
     */
    public function populateFromArray(array $data);
}
