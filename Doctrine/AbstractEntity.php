<?php
namespace Docs\CommonBundle\Doctrine;

/**
 * Abstract entity class
 * @author Hristo Botev
 *
 */
abstract class AbstractEntity implements EntityInterface
{
    /**
     * Provide automatic getter and setter methods for
     * all properties of the object
     * @param string $method
     * @param array $args
     * @throws \BadMethodCallException
     * @return mixed
     */
    public function __call($method, $args)
    {
        $operation = substr($method, 0, 3);

        if ($operation != "get" && $operation != "set") {
            throw new \BadMethodCallException(
                "Call to undefined method " . get_class($this) . "::" . $method . "()"
            );
        }

        $property = lcfirst(substr($method, 3));

        if (!property_exists($this, $property)) {
            throw new \BadMethodCallException(
                "Class " . get_class($this) . " doesn't have a property " . $property
            );
        }

        if ($operation == "get") {
            return $this->$property;
        }


        $this->$property = $args[0];

        return $this;
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        throw new \Exception('Trying to access invalid property of class: ' . get_class($this));
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->{"set" . ucfirst($name)}($value);
            return $this;
        }

        throw new \Exception('Trying to set non existent property of class: ' . get_class($this));
    }

    /**
     * @param array $data
     * @throws \InvalidArgumentException
     * @return \Docs\CommonBundle\Doctrine\AbstractEntity
     */
    public function populateFromArray(array $data)
    {
        if (!count($data)) {
            throw new \InvalidArgumentException('Cannot populate entity from empty data');
        }

        $reflection = new \ReflectionClass($this);

        foreach ($data as $association => $value) {
            $setterName = "set" . ucfirst($association);

            if (!$reflection->hasProperty($association)) {
                continue;
            }

            $this->$setterName($value);
        }

        return $this;
    }
}
