<?php
namespace Docs\CommonBundle\Doctrine\Repository;

/**
 * Parts of this class were written by colleges of mine
 * but it's so handy that it's a shame not to use it
 */

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Docs\CommonBundle\Utils\Utils;
use Docs\CommonBundle\Doctrine\Mapping\Schema;
use Docs\CommonBundle\Doctrine\EntityInterface;
use Docs\CommonBundle\Doctrine\Mapping\Validator;

/**
 * Abstract repository lass
 */
abstract class AbstractRepository extends EntityRepository
{

    const OPERATION_READ = 'read';
    const OPERATION_SAVE = 'save';

    /**
     * Reset all options in the query builder instance
     * @var int
     */
    const RESET_ALL = 1;

    /**
     * Reset where options in the query builder instance
     * @var int
     */
    const RESET_WHERE = 2;

    /**
     * Reset Order options in the query builder instance
     * @var int
     */
    const RESET_ORDER = 3;

    /**
     * Flush the persisted entity when saving an entity
     * @var int
     */
    const SAVE_ENTITY = 1;

    /**
     * Only persists the entity
     * @var int
     */
    const PERSIST_ENTITY = 2;

    /**
     * @var boolean Flag for repository auto transactional state
     */
    protected $autoTranasctional = true;

    /**
     * @var \Doctrine\ORM\QueryBuilder Doctrine ORM Query builder instance
     */
    protected $queryBuilder = null;

    /**
     * @var boolean If you need to use attribute accessors in your entity
     */
    protected $useAttributeAccessors = false;

    /**
     * @var array Array of where conditions
     */
    protected $where = array();

    /**
     * @var array Array for order statements
     */
    protected $order = array();

    /**
     * Get related entities after performing some validations
     *
     * @return array
     */
    final protected function getEntitiesMap($operation = self::OPERATION_READ)
    {
        return isset($this->entitiesMap[$operation]) ? $this->entitiesMap[$operation] : array();
    }

    /**
     * Set if the repository should start its own transaction
     *
     * @param boolean $flag
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     */
    public function setAutoTransactional($flag)
    {
        $this->autoTranasctional = (boolean) $flag;
        return $this;
    }

    /**
     * Get the repository auto transactional flag
     * Default the repository will persist entities in transaction
     *
     * @return boolean
     */
    public function isAutoTransactional()
    {
        return (boolean) $this->autoTranasctional;
    }

    /**
     * Constructor.
     *
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Doctrine\ORM\Mapping\ClassMetadata $metadata
     */
    public function __construct(EntityManager $em, ClassMetadata $metadata)
    {
        if (!isset($this->entitiesMap)) {
            throw new Exception("Entity map missing for entity `{$metadata->getName()}`");
        }

        $validator = new Validator();

        $validator->validateMapping($this->entitiesMap);

        parent::__construct($em, $metadata);
    }

    /**
     * Get assoc array of entity's map
     *
     * @param int $operation
     * @return array
     */
    protected function getEntities($operation = self::OPERATION_READ)
    {
        $entities = $this->getEntitiesMap($operation);
        //  if missing map for operation save :: get the default
        if (empty($entities) && $operation !== self::OPERATION_READ) {
            $entities = $this->getEntitiesMap();
        }

        $primary = $this->getEntityName();

        return array(
            $this->getEntityAlias($primary) => array(
                Schema::$entityKey       => $primary,
                Schema::$associationsKey => $entities
            )
        );
    }

    /**
     * Get the entity class name only as alias for saving through array
     *
     * @param string $entityName
     * @return string
     */
    protected function getEntityAlias($entityName)
    {
        if (!is_string($entityName)) {
            throw new InvalidArgumentException(
                sprintf('%s expects string parameter, %s given!', __METHOD__, gettype($entityName))
            );
        }

        $parts       = explode('\\', $entityName);
        $entityAlias = end($parts);

        return $entityAlias;
    }

    /**
     * Save entity
     * @param \Docs\CommonBundle\Doctrine\EntityInterface|array $entityOrArray
     * @param int $type
     *
     * Example:
     * $repository->save($array, Repostitory::SAVE_ENTITY)
     *
     * $repository->save($entity)
     *
     * @return array
     */
    public function save($entityOrArray, $type = '')
    {
        if ($entityOrArray instanceof EntityInterface) {
            return $this->saveEntity($entityOrArray);
        }

        if (!is_array($entityOrArray)) {
            throw new InvalidArgumentException('You can save an entity only by sending an associative array');
        }

        if (empty($type)) {
            throw new InvalidArgumentException('When saving from an array - Type of the action is required - Persist/Save');
        }

        $entityManager = $this->getEntityManager();

        $entityName = $this->getEntityName();
        $entity = new $entityName;
        /* @var $entity \Docs\CommonBundle\Doctrine\AbstractEntity */

        $entity->populateFromArray($entityOrArray);

        if ($type === self::PERSIST_ENTITY) {
            $entityManager->persist($entity);
            return $entity;
        }

        try {
            $entityManager->persist($entity);
            $entityManager->flush();
        } catch (Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->clear();

            throw new Exception("Saving entity through associative array failed", null, $e);
        }
    }

    /**
     * Save fully populated entity.
     *
     * @param \Docs\CommonBundle\Doctrine\EntityInterface $entity
     * @return \Docs\CommonBundle\Doctrine\EntityInterface
     * @throws Exception
     */
    public function saveEntity(EntityInterface $entity)
    {

        $entities = $this->getEntities(self::OPERATION_SAVE);

        try {
            //  begin transaction
            if ($this->isAutoTransactional()) {
                $this->getEntityManager()->getConnection()->beginTransaction();
            }

            $this->getEntityManager()->persist($entity);

            $current = current($entities);

            $this->prepareEntitySave($entity, $current);

            //  commit transaction
            if ($this->isAutoTransactional()) {
                $this->getEntityManager()->flush();
                $this->getEntityManager()->getConnection()->commit();
            }
        } catch (\Exception $e) {
            //  rollback transaction and clear entity manager
            if ($this->isAutoTransactional()) {
                $this->getEntityManager()->getConnection()->rollback();
                $this->getEntityManager()->clear();
            }

            throw new Exception('Saving newly populated entity failed', null, $e);
        }

        return $entity;
    }

    /**
     * Prepares the populated entity for saving recursively
     * Recursive method.
     *
     * @param \Docs\CommonBundle\Doctrine\EntityInterface $entity
     * @param array $entities
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     */
    protected function prepareEntitySave(EntityInterface $entity, array $entities)
    {
        if (empty($entities[Schema::$associationsKey])) {
            return $this;
        }

        foreach ($entities[Schema::$associationsKey] as $association => $entityMetadata) {
            if (!is_array($entityMetadata)) {
                continue;
            }

            $method = "get" . ucfirst($association);
            //  this won't work for implementations with magic method
            if (!is_callable(array($entity, $method))) {
                continue;
            }
            try {
                $children = $entity->$method();
            } catch (\BadMethodCallException $e) {
                throw new \InvalidPropertyException(
                    sprintf("Association requested is missing for entity ``", get_class($entity)),
                    null,
                    $e
                );
            }
            //  persistent collection is collection from already persisted entities
            if ($children instanceof \Doctrine\ORM\PersistentCollection) {
                foreach ($children as $child) {
                    $this->getEntityManager()->persist($child);

                    $this->prepareEntitySave($child, $entityMetadata);
                }
            } elseif ($children instanceof \Doctrine\Common\Collections\ArrayCollection) {
                //  ArrayCollection is collection of fresh new not yet persisted entities
                continue;
            } else {
                $this->getEntityManager()->persist($children);
                //  persist the relation as well
                $this->prepareEntitySave($children, $entityMetadata);
            }
        }
        return $this;
    }

    /**
     * Delete front end marked entities
     *
     * @todo Fixme
     * @param type $data
     * @param type $asociation
     * @return Docs\Common\Doctrine\Repository\RepositoryAbstract
     * @throws Exception
     */
    protected function deleteMarkedData($data, $asociation)
    {
        $deleteConditions = array();
        $i                = 0;
        foreach ($data as $key => $data) {
            $elementNameArray = explode('_', $key);

            if (end($elementNameArray) !== 'delete') {
                continue;
            }

            if (Utils::isEmptyString($data)) {
                continue;
            }

            $deleteComponentsData = explode('/', $data);

            foreach ($deleteComponentsData as $deleteComponent) {
                $deleteComponent = substr($deleteComponent, 1, -1);

                if (Utils::isEmptyString($deleteComponent)) {
                    continue;
                }

                $componentsDeleteData = explode(',', $deleteComponent);

                foreach ($componentsDeleteData as $componentDeleteData) {
                    $deleteDataParts = explode('=', $componentDeleteData);

                    if (!isset($deleteDataParts[0]) || !isset($deleteDataParts[1])) {
                        throw new Exception('Tring to delete some entity, but there in not right determine in form');
                    }

                    $associationData = $deleteDataParts[0];
                    $identifierValue = $deleteDataParts[1];

                    $sortingPatern = "'\[([a-z]*?)\]'si";

                    preg_match_all($sortingPatern, $associationData, $matches);

                    $nameOfIdentifier = array_pop($matches[1]);

                    $nameOfAssociation = array_shift($matches[1]);

                    if (!empty($matches[1])) {
                        foreach ($matches[1] as $associationOrder) {
                            $nameOfAssociation .= '_' . $associationOrder;
                        }
                    }

                    $deleteConditions[$i][$nameOfAssociation][$nameOfIdentifier] = $identifierValue;
                }
                $i++;
            }
        }
        if (empty($deleteConditions)) {
            return $this;
        }

        foreach ($deleteConditions as $condition) {
            $associationNames = array_keys($condition);


            $leadingAssociation = $this->getMarkedDataLeadingAssociation($associationNames, $asociation);

            $identifiers = $this->getEntityManager()
                    ->getClassMetadata($leadingAssociation[Schema::$entityKey])
                    ->getIdentifierFieldNames();

            $matchedKeys = array_intersect_key($condition[$leadingAssociation['name']], array_flip($identifiers));

            if (count($matchedKeys) != count($identifiers)) {
                throw new Exception('Not enough data given for the entity you want to delete!');
            }

            if (in_array(null, $matchedKeys)) {
                continue;
            }

            $entity = $this->getEntityManager()
                    ->getRepository($leadingAssociation[Schema::$entityKey])
                    ->findOneBy($matchedKeys);

            $this->getEntityManager()->remove($entity);
        }
    }

    /**
     * @todo Fixme
     * @param array $associationNames
     * @param array $asociation
     * @return array
     * @throws Exception
     */
    protected function getMarkedDataLeadingAssociation($associationNames, $asociation)
    {

        if (!is_array($associationNames) || empty($associationNames)) {
            throw new Exception('The names for the marked for delete associations were not given');
        }

        foreach ($associationNames as $name) {
            $nameArray = explode('_', $name);

            $topAssociation = null;
            if (!isset($topAssociation)) {
                $topAssociation = $nameArray;
                continue;
            }

            if (count($nameArray) < count($topAssociation)) {
                $topAssociation = $nameArray;
            }
        }

        foreach ($topAssociation as $associationName) {
            if (!isset($asociation->$associationName)) {
                throw new Exception('Wrong data given for the deleted association');
            }

            $asociation = $asociation->$associationName;
        }
        if (!isset($asociation[Schema::$entityKey])) {
            throw new Exception('Entity associations mapping seems to be invalid');
        }

        return array(
            Schema::$entityKey => $asociation[Schema::$entityKey],
            'name'             => implode('_', $topAssociation)
        );
    }

    protected function populateCompositeEntity(EntityInterface $entity, array $data, $relation)
    {
        if ($this->useAttributeAccessors) {
            $this->populateAttributeAccessors($entity, $data);
        }

        $metadata = $this->getEntityManager()->getClassMetadata(get_class($entity));

        $properties = $metadata->getFieldNames();

        $entityData = array_intersect_key($data, array_flip($properties));

        $entity->fromArray($entityData);
        //  for entity's relations is not mandatory to have their associations mapped
        if (isset($relation[Schema::$associationsKey])) {
            //  otherwise...
            foreach ($relation[Schema::$associationsKey] as $association => $associationData) {
                if (!is_array($associationData)) {
                    continue;
                }

                $required = 0;
                if (isset($associationData[Schema::$requiredKey]) && true == $associationData[Schema::$requiredKey]) {
                    $required = 1;
                }

                if (!isset($data[$association]) && 1 == $required) {
                    throw new Exception('No data given for required association `' . $association . '`.');
                }

                if (1 == $required && !count($data[$association])) {
                    throw new Exception("Trying to save without all the required data for `$association` association.");
                }

                $associationMaping = $metadata->getAssociationMapping(lcfirst($association));

                if (!isset($data[$association]) && 0 == $required) {
                    continue;
                }

                if (empty($data[$association]) && 0 == $required) {
                    if ($associationMaping['type'] == ClassMetadataInfo::MANY_TO_MANY) {
                        $entity->{'get' . ucfirst($association)}()->clear();
                    }

                    if (($associationMaping['type'] == ClassMetadataInfo::ONE_TO_ONE && $associationMaping['isOwningSide']) || $associationMaping['type'] == ClassMetadataInfo::MANY_TO_ONE) {
                        $entity->{'set' . ucfirst($association)}(null);
                    }

                    continue;
                }

                if ($associationMaping['type'] == ClassMetadataInfo::ONE_TO_ONE || $associationMaping['type'] == ClassMetadataInfo::MANY_TO_ONE
                ) {
                    $this->bindToOneAssociation($entity, $data[$association], $association, $relation);
                } else {
                    $this->bindToManyAsociation($entity, $data[$association], $association, $relation);
                }
            }
        }

        $this->getEntityManager()->persist($entity);

        return $entity;
    }

    protected function bindToManyAsociation(EntityInterface $entity, array $data, $association, $relation)
    {

        if (!is_array($data)) {
            throw new Exception(sprintf("`ToMany` association data must be an array, but `%s` given", gettype($data)));
        }

        if (empty($data)) {
            throw new Exception('There is no data for composite entity!');
        }

        $metaDataEntity = $this->getEntityManager()->getClassMetadata(get_class($entity));

        $entityAssociationMapping = $metaDataEntity->getAssociationMapping(lcfirst($association));

        $metaDataTarget = $this->getEntityManager()->getClassMetadata($entityAssociationMapping['targetEntity']);

        $biDirectional     = true;
        $targetIdentifires = array_flip($metaDataTarget->getIdentifierFieldNames());
        if ($entityAssociationMapping['isOwningSide']) {
            $addAssociationName = $entityAssociationMapping['inversedBy'];

            if (empty($addAssociationName)) {
                $biDirectional = false;
            }
        } else {
            $addAssociationName = $entityAssociationMapping['mappedBy'];
        }

        if ($biDirectional) {
            $targetEntityAssociationMapping = $metaDataTarget->getAssociationMapping($addAssociationName);
        }

        $searchedEntityName = $entityAssociationMapping['targetEntity'];

        $addedAssociations = array();

        foreach ($data as $key => $value) {
            if (!Utils::isUInt($key)) {
                throw new Exception('`ToMany` association data array must be numeric array!');
            }

            if (!is_array($value)) {
                throw new Exception('For select or check box we must set the name of the identifier fields!');
            }

            $matchedKeys = array_intersect_key($value, $targetIdentifires);
            //  if no primary key is sent regardless of empty value
            //  will throw an error
            if (count($matchedKeys) != count($targetIdentifires)) {
                throw new Exception('Number of identifiers is not equal with table identifiers!');
            }
            //  if the primary key value is not empty
            //  will try to to find the related entity and work with it
            if (count($value) == count($targetIdentifires)) {
                $searchedEntity = $this->getEntityManager()
                        ->getRepository($searchedEntityName)
                        ->findOneBy($matchedKeys);
                //  if no entity is found for this id
                //  an error will be thrown
                if (null == $searchedEntity) {
                    throw new Exception('When editing checkbox or select association can not create new entities');
                }
                //  mark as added association
                $addedAssociations[] = $searchedEntity;
                //  if already in the entity's collection will continue with next
                if ($entity->{'get' . ucfirst($association)}()->contains($searchedEntity)) {
                    continue;
                }

                $entity->{'get' . ucfirst($association)}()->add($searchedEntity);

                if ($biDirectional) {
                    //  if relationship in the inverse is `toMany` - add it to the inverse collection too
                    if ($targetEntityAssociationMapping['type'] == ClassMetadataInfo::MANY_TO_MANY) {
                        $searchedEntity->{'get' . ucfirst($addAssociationName)}()->add($entity);
                        continue;
                    }
                    //  else add it as main association as relationship `toOne`
                    $searchedEntity->{'set' . ucfirst($addAssociationName)}($entity);
                }
                continue;
            }

            $entityIdentifires = $metaDataEntity->getIdentifierValues($entity);

            $checkSaveEntityIdentifires = in_array(null, $entityIdentifires);
            //  for empty primary keys new related entity will be added
            if ($checkSaveEntityIdentifires || in_array(null, $matchedKeys)) {
                $searchedEntity = new $searchedEntityName;

                $searchedEntity = $this->populateCompositeEntity($searchedEntity, $value, $relation[Schema::$associationsKey][$association]);

                //  newly created entity will always be added to our entity's collection
                $entity->{'get' . ucfirst($association)}()->add($searchedEntity);

                if ($biDirectional) {
                    if ($targetEntityAssociationMapping['type'] == ClassMetadataInfo::MANY_TO_MANY) {
                        $searchedEntity->{'get' . ucfirst($addAssociationName)}()->add($entity);
                        continue;
                    }
                    $searchedEntity->{'set' . ucfirst($addAssociationName)}($entity);
                }
                continue;
            }

            $searchedEntity = $this->getEntityManager()
                    ->getRepository($searchedEntityName)
                    ->findOneBy($matchedKeys);

            $isInTheCollection = $entity->{'get' . ucfirst($association)}()->contains($searchedEntity);

            $searchedEntity = $this->populateCompositeEntity($searchedEntity, $value, $relation[Schema::$associationsKey][$association]);

            if (!$isInTheCollection) {
                $entity->{'get' . ucfirst($association)}()->add($searchedEntity);

                if ($biDirectional) {
                    if ($targetEntityAssociationMapping['type'] == ClassMetadataInfo::MANY_TO_MANY) {
                        $searchedEntity->{'get' . ucfirst($addAssociationName)}()->add($entity);
                        continue;
                    }

                    $searchedEntity->{'set' . ucfirst($addAssociationName)}($entity);
                }
            }
        }
        //  if associations has been added (excluding the newly created ones)
        //  attempt to diff between existing and added will be made
        if (!empty($addedAssociations)) {
            $existingAssociations = $entity->{'get' . ucfirst($association)}();
            //  if existing association is not found in the added associations
            //  it will be removed from the collection
            foreach ($existingAssociations as $existing) {

                if (!in_array($existing, $addedAssociations)) {
                    $entity->{'get' . ucfirst($association)}()->removeElement($existing);
                    //  if the relation is bi-directional
                    //  relation will be removed from the inversed side too
                    if ($biDirectional) {
                        $existing->{'get' . ucfirst($addAssociationName)}()->removeElement($entity);
                    }
                }
            }
        }
        return $this;
    }

    protected function bindToOneAssociation(EntityInterface $entity, array $data, $association, $relation)
    {
        if (!is_array($data)) {
            throw new Exception('`ToOne` association data must be an array!');
        }

        $metaDataEntity = $this->getEntityManager()->getClassMetadata(get_class($entity));

        $entityAssociationMapping = $metaDataEntity->getAssociationMapping(lcfirst($association));

        if (empty($entityAssociationMapping) || empty($entityAssociationMapping['targetEntity'])) {
            throw new Exception('Can not find association for relationship `ToOne` !');
        }

        $metaDataTarget = $this->getEntityManager()->getClassMetadata($entityAssociationMapping['targetEntity']);

        $biDirectional = true;
        $identifires   = array_flip($metaDataTarget->getIdentifierFieldNames());
        if ($entityAssociationMapping['isOwningSide']) {
            $addAssociationName = $entityAssociationMapping['inversedBy'];

            if (empty($addAssociationName)) {
                $biDirectional = false;
            }
        } else {
            $addAssociationName = $entityAssociationMapping['mappedBy'];
        }

        $searchedEntityName = $entityAssociationMapping['targetEntity'];


        $dataKeys = array_keys($data);
        if (count($data) == 1 && Utils::isUInt($dataKeys[0])) {
            $data = array_shift($data);
        }

        $matchedKeys = array_intersect_key($data, $identifires);
        if (count($matchedKeys) != count($identifires)) {
            throw new Exception("Number of Identifiers is not equal with table identifiers for `{$entityAssociationMapping['targetEntity']}`entity!");
        }

        if (count($data) == count($identifires)) {

            $searchedEntity = $this->getEntityManager()
                    ->getRepository($searchedEntityName)
                    ->findOneBy($matchedKeys);

            if (null == $searchedEntity) {
                throw new Exception('When editing checkbox or select association can not create new entities');
            }

            $entity->{'set' . ucfirst($association)}($searchedEntity);

            if (false === $biDirectional) {
                return $this;
            }

            $addEntityMapping = $metaDataTarget->getAssociationMapping($addAssociationName);

            if ($addEntityMapping['type'] == ClassMetadataInfo::ONE_TO_MANY) {
                $searchedEntity->{'get' . ucfirst($addAssociationName)}()->add($entity);
                return $this;
            }
            $searchedEntity->{'set' . ucfirst($addAssociationName)}($entity);
            return $this;
        }

        $searchedEntity = $this->getEntityManager()->getRepository($searchedEntityName)->findOneBy($matchedKeys);

        if (empty($searchedEntity)) {
            $searchedEntity = new $searchedEntityName;
        }

        $searchedEntity = $this->populateCompositeEntity($searchedEntity, $data, $relation[Schema::$associationsKey][$association]);

        $entity->{'set' . ucfirst($association)}($searchedEntity);

        if (false === $biDirectional) {
            return $this;
        }

        $addEntityMapping = $metaDataTarget->getAssociationMapping($addAssociationName);
        if ($addEntityMapping['type'] == ClassMetadataInfo::ONE_TO_MANY) {

            $searchedEntity->{'get' . ucfirst($addAssociationName)}()->add($entity);
            return $this;
        }
        $searchedEntity->{'set' . ucfirst($addAssociationName)}($entity);

        return $this;
    }

    protected function getStartEntity($operation)
    {
        $entityName  = $this->getEntityName();
        $entityAlias = $this->getEntityAlias($entityName);

        return array(Schema::$entityKey => $entityName, Schema::$aliasKey => $entityAlias);
    }

    /**
     * Get instance of Query Builder
     *
     * @return Docs\Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        if (null === $this->queryBuilder) {
            $this->queryBuilder = $this->getEntityManager()->createQueryBuilder();

            $this->buildStatement($this->queryBuilder);
        }

        return $this->queryBuilder;
    }

    /**
     * Get cloned instance of the query buidler
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function cloneQueryBuilder()
    {
        return clone $this->getQueryBuilder();
    }

    /**
     * Build DQL with all set of conditions and return query builder
     *
     * @param \Doctrine\ORM\QueryBuilder $qb
     * @param array $entitiesMap = null
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function buildStatement(QueryBuilder $qb, $entitiesMap = null)
    {

        if ($qb->getDQLPart('select')) {
            return $qb;
        }

        if (null === $entitiesMap) {
            $entities = $this->getEntities(self::OPERATION_READ);
        } else {
            $entities = $entitiesMap;
        }

        $entities    = current($entities);
        $entityName  = $entities[Schema::$entityKey];
        $entityAlias = $this->getEntityAlias($entityName);

        $qb->from($entityName, $entityAlias);

        //   Construct join statements
        $qb = $this->constructJoin($qb, $entities);

        //  Build where conditions
        $qb = $this->buildConditions($qb);

        return $qb;
    }

    /**
     * Get Entity alias by entity name
     *
     * @param string $entityName
     * @return string
     * @throws Exception
     */
    protected function getAliasByEntityName($entityName)
    {

        if (Utils::isEmptyString($entityName) && !is_array($entityName)) {
            throw new Exception('Entity name can not be emtpy!');
        }

        if (is_array($entityName) && isset($entityName[Schema::$aliasKey])) {
            return $entityName[Schema::$aliasKey];
        }

        return $entityName;
    }

    /**
     * Construct join conditions
     *
     * @param array $entities
     * @param \Doctrine\ORM\QueryBuilder $qb
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function constructJoin(QueryBuilder $qb, $entities)
    {
        /**
         * Get the metaData for the entity
         * @var Doctrine\ORM\Mapping\ClassMetadata
         */
        $classMethadata = $this->getEntityManager()->getClassMetadata($entities[Schema::$entityKey]);

        if (isset($entities[Schema::$aliasKey])) {
            $entityAlias = $entities[Schema::$aliasKey];
        } else {
            $entityAlias = $this->getEntityAlias($entities[Schema::$entityKey]);
        }


        $qb->addSelect($entityAlias);

        if (empty($entities[Schema::$associationsKey])) {
            return $qb;
        }

        foreach ($entities[Schema::$associationsKey] as $key => $entity) {

            if (!is_array($entity)) {
                continue;
            }

            /**
             * When you try to getAssociationMapping from meta data
             * if it doesn't exist it trows an exeption
             */
            try {
                $classMethadata->getAssociationMapping(lcfirst($key));
            } catch (Exception $e) {
                throw new Exception('Association set in the repository config does not exist!');
            }

            if (isset($entity[Schema::$aliasKey])) {
                $joinAlias = $entity[Schema::$aliasKey];
            } else {
                $joinAlias = $this->getEntityAlias($entity[Schema::$entityKey]);
            }

            /**
             * If flag isToMany is set and value is true join table.
             * This is required for proper where clauses
             */
            if (isset($entity[Schema::$isToManyKey]) && true == $entity[Schema::$isToManyKey] && $this->hasWhereByAlias($joinAlias)) {
                $qb->leftJoin($entityAlias . '.' . lcfirst($key), $joinAlias);

                $this->constructJoin($qb, $entity);
                continue;
            } elseif (isset($entity[Schema::$isToManyKey]) && true == $entity[Schema::$isToManyKey] && false == $this->hasWhereByAlias($joinAlias)) {
                continue;
            }

            if (isset($entity[Schema::$requiredKey]) && true == $entity[Schema::$requiredKey]) {
                $qb->innerJoin($entityAlias . '.' . lcfirst($key), $joinAlias);
            } else {
                $qb->leftJoin($entityAlias . '.' . lcfirst($key), $joinAlias);
            }

            $this->constructJoin($qb, $entity);
        }

        return $qb;
    }

    /**
     * Build all where and order conditions
     *
     * @param \Doctrine\ORM\QueryBuilder $qb
     * @return \Doctrine\ORM\QueryBuilder
     * @throws InvalidArgumentException
     * @throws Exception
     */
    protected function buildConditions(QueryBuilder $qb)
    {
        $where = $this->getWhere();

        if (count($where)) {
            $i = 0;
            foreach ($where as $condition) {

                $paramAssociation = "param_{$i}";
                $expr             = $this->getConditionObject($condition, $paramAssociation);

                if (null == $expr) {
                    throw new Exception('Invalid operation added in where statement');
                }

                if ($expr instanceof \Doctrine\ORM\Query\Expr\Comparison) {
                    $qb->andWhere($expr);
                    $qb->setParameter($paramAssociation, $condition['value'], $condition['type']);
                } else {
                    /**
                     * Expression returned
                     */
                    $exprReflection = new \ReflectionClass('Doctrine\ORM\Query\Expr');

                    if (!$exprReflection->hasMethod($condition['operation'])) {
                        throw new InvalidArgumentException("Invalid operation `{$condition['operation']}` for comparison set in the conditions");
                    }
                    $qb->andWhere($expr);

                    $parameterNumber = $exprReflection
                            ->getMethod($condition['operation'])
                            ->getNumberOfParameters();

                    $expr = new \Doctrine\ORM\Query\Expr();
                    if ($parameterNumber == 2) {
                        $qb->setParameter($paramAssociation, $condition['value'], $condition['type']);
                    } elseif ($parameterNumber > 2 && is_array($condition['value'])) {
                        $valuesCount = count($condition['value']);
                        $values      = array_values($condition['value']);
                        for ($i = 0; $i < $valuesCount; $i++) {
                            $qb->setParameter("{$paramAssociation}_{$i}", $values[$i], $condition['type']);
                        }
                    }
                }
                ++$i;
            }
        }

        $orders = $this->getOrder();

        if (!count($orders)) {
            return $qb;
        }

        foreach ($orders as $field => $orderDirection) {
            $qb->addOrderBy($field, $orderDirection);
        }

        return $qb;
    }

    /**
     * Get the field type by looking in the entity annotation
     *
     * @param string $field
     * @param string | integer $value
     * @param string $entityName
     * @return string
     */
    public function getFieldType($field, $value, $entityName = null)
    {
        $alias = null;
        if (null !== $entityName) {
            $alias = $this->getAliasByEntityName($entityName);
        }

        if (null === $alias) {
            $alias = $this->getAliasByEntityName($this->getStartEntity(self::OPERATION_READ));
        }
        $type = null;

        $realEntityName = $this->getEntityNameFromAlias($alias);

        if (null != $realEntityName && !is_array($value)) {
            if ($this->getEntityManager()->getClassMetadata($realEntityName)->hasField($field)) {
                $fieldMapping = $this->getEntityManager()
                        ->getClassMetadata($realEntityName)
                        ->getFieldMapping($field);
                $type         = $fieldMapping['type'];
            }
        }

        return $type;
    }

    /**
     * Get condition expresion by condition array
     *
     * @param array $condition
     * @param string $paramAssociation
     * @throws Exception
     */
    protected function getConditionObject($condition, $paramAssociation)
    {
        $comparationReflection = new \ReflectionClass('Doctrine\ORM\Query\Expr\Comparison');

        if ($comparationReflection->hasConstant($condition['operation'])) {
            $operation = $comparationReflection->getConstant($condition['operation']);

            return new \Doctrine\ORM\Query\Expr\Comparison($condition['field'], $operation, ":{$paramAssociation}");
        }

        $exprReflection = new \ReflectionClass('Doctrine\ORM\Query\Expr');

        if ($exprReflection->hasMethod($condition['operation'])) {
            $parameterNumber = $exprReflection->getMethod($condition['operation'])
                    ->getNumberOfParameters();
            $expr            = new \Doctrine\ORM\Query\Expr();
            if ($parameterNumber == 1) {
                $expression = $expr->$condition['operation']($condition['field']);
            } elseif ($parameterNumber == 2) {
                $expression = $expr->$condition['operation']($condition['field'], ":{$paramAssociation}");
            } elseif ($parameterNumber > 2 && is_array($condition['value'])) {
                $valuesCount      = count($condition['value']);
                $expressionParams = array($condition['field']);
                for ($i = 0; $i < $valuesCount; $i++) {
                    $expressionParams[] = ":{$paramAssociation}_{$i}";
                }
                $expressionMethod = $exprReflection->getMethod($condition['operation']);
                $expression       = $expressionMethod->invokeArgs($expr, $expressionParams);
            } else {
                throw new Exception('Invalid where conditions set!');
            }

            return $expression;
        }

        return null;
    }

    /**
     * Get the entity name from given entity alias
     *
     * @param string $alias
     * @param array $entities
     * @return null | string
     */
    protected function getEntityNameFromAlias($alias, $entities = null)
    {

        if (null == $entities) {
            $entities = $this->getEntities(self::OPERATION_READ);
        }
        $currentEntity = key($entities);
        //  return alias key first if is equal
        if (isset($entities[$currentEntity][Schema::$aliasKey]) && $alias == $entities[$currentEntity][Schema::$aliasKey]) {
            return $entities[$currentEntity][Schema::$aliasKey];
        }
        //  if the array key is the same return the entity key
        if ($currentEntity == $alias) {
            return $entities[$currentEntity][Schema::$entityKey];
        }

        if (!isset($entities[$currentEntity][Schema::$associationsKey])) {
            return null;
        }

        foreach ($entities[$currentEntity][Schema::$associationsKey] as $key => $entity) {
            if (is_array($entity)) {
                return $this->getEntityNameFromAlias($alias, array($key => $entity));
            }
        }

        return null;
    }

    /**
     * Check conditions in where clause for entity
     *
     * @param string $alias
     * @return boolean
     */
    protected function hasWhereByAlias($alias)
    {
        $whereConditions = $this->getWhere();

        if (empty($whereConditions)) {
            return false;
        }

        foreach ($whereConditions as $condition) {
            $pattern = "/$alias\..*/";

            if (preg_match($pattern, $condition['field'])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get All where conditions
     *
     * @return array
     */
    public function getWhere()
    {
        return $this->where;
    }

    /**
     * Set the where clauses from array
     * array( '0' => array(
     *                 'filed' => fieldName,
     *                 'value' => value,
     *                 'operator' => 'sql where clause operator (=, LIKE, etc.)'))
     *
     * @param array $conditions
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     * @throws Exception
     */
    public function setWhere(array $conditions)
    {

        if (empty($conditions)) {
            throw new Exception('Conditions for set where can not be empty array');
        }

        foreach ($conditions as $condition) {

            if (!is_array($condition)) {
                throw new Exception('Invalid array structure!');
            }

            if (empty($condition)) {
                throw new Exception('Invalid array structure!');
            }

            if (!isset($condition['field']) || !isset($condition['value']) || !isset($condition['operator'])) {
                throw new Exception('Invalid array structure!');
            }

            $entityName = null;

            if (isset($condition['entity'])) {
                $entityName = $condition['entity'];
            }

            $label = null;

            if (isset($condition['label'])) {
                $label = $condition['label'];
            }

            $this->addWhere($condition['field'], $condition['value'], $condition['operator'], $entityName, $label);
        }

        return $this;
    }

    /**
     * Add where condition on entities
     *
     * @param string $field The field to filter
     * @param string|int $value The value to filter against
     * @param \Doctrine\ORM\Query\Expr\Comparison $operation Operation of comparison
     * @param string $entityName The entity name that the field belongs to ( defaults to current repository owner )
     * @param string $label The label for this condition Additional label for this filter operation
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     */
    public function addWhere($field, $value = null, $operation = "EQ", $entityName = null, $label = null)
    {

        $alias = null;
        if (null !== $entityName) {
            $alias = $this->getEntityAlias($entityName);
        }

        if (null === $alias) {
            $alias = $this->getAliasByEntityName($this->getStartEntity(self::OPERATION_READ));
        }

        $type = null;

        $realEntityName = $this->getEntityNameFromAlias($alias);
        if (null != $realEntityName && !is_array($value)) {

            if ($this->getEntityManager()->getClassMetadata($realEntityName)->hasField($field)) {
                $fieldMapping = $this->getEntityManager()
                        ->getClassMetadata($realEntityName)
                        ->getFieldMapping($field);

                $type = $fieldMapping['type'];
            }
        }

        $this->where[] = array(
            'field'     => $alias . '.' . $field,
            'value'     => $value,
            'operation' => $operation,
            'type'      => $type,
            'label'     => $label
        );

        return $this;
    }

    /**
     * Get `Order` statements
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set `Order` statements
     *
     * @param array $orderDirections
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     * @throws Exception
     */
    public function setOrder($orderDirections)
    {

        if (!is_array($orderDirections)) {
            throw new Exception('Condiotions must be array!');
        }

        if (!count($orderDirections)) {
            throw new Exception('Conditions can not be empty!');
        }

        foreach ($orderDirections as $order) {

            if (!isset($order['field']) || !isset($order['direction'])) {
                throw new Exception('Invalid array structure for order clause!');
            }

            $field      = $order['field'];
            $direction  = $order['direction'];
            $entityName = null;

            if (isset($order['entity'])) {
                $entityName = $order['entity'];
            }

            $this->addOrder($field, $direction, $entityName);
        }

        return $this;
    }

    /**
     * Add order on current object
     *
     * @param string $field
     * @param string $orderDirection
     * @param string $entityName
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     * @throws Exception
     */
    public function addOrder($field, $orderDirection, $entityName = null)
    {

        $alias = null;

        if (null != $entityName) {
            $alias = $this->getEntityAlias($entityName);
        }

        if (null === $alias) {
            $alias = $this->getAliasByEntityName($this->getStartEntity(self::OPERATION_READ));
        }

        if (in_array($field, array_keys($this->order))) {
            throw new Exception('Order clause is already exist!');
        }

        $this->order[$alias . '.' . $field] = $orderDirection;

        return $this;
    }

    /**
     * Populate attribute accsesors (that's the fields which exists only in the model
     * and you can't find them in database)
     *
     * @param \Docs\CommonBundle\Doctrine\EntityInterface $entity
     * @param array $data Array( fieldname => fieldvalue )
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     */
    protected function populateAttributeAccessors(EntityInterface $entity, array $data)
    {
        $refEntity = new \ReflectionClass(get_class($entity));

        $entityProperties = $refEntity->getDefaultProperties();

        $attrAccessorsArray = array();

        foreach ($entityProperties as $name => $value) {
            $property = new \ReflectionProperty(get_class($entity), $name);
            $doc      = $property->getDocComment()->getContents();
            if (strstr($doc, "@attributeAccessor")) {
                $attrAccessorsArray[] = $name;
            }
        }

        $valuesToPopulate = array_intersect_key($data, array_flip($attrAccessorsArray));

        foreach ($valuesToPopulate as $fieldName => $value) {
            $propName = ucfirst($fieldName);
            $set      = "set{$propName}";
            $entity->$set($value);
        }

        return $this;
    }

    /**
     * Delete from start entity.
     *
     * Examples
     * array(0 => id)
     * array(0 => array('username' => array('value' => id)))
     * array('username' => array('value' => id)
     *
     * @param mixed $params
     * @throws Exception
     */
    public function delete($params)
    {

        if (empty($params)) {
            throw new Exception('Delete params can not be empty!');
        }

        if (!is_array($params)) {
            $params = array($params);
        }

        if (!count($params)) {
            return false;
        }

        $primaryEntity = $this->getStartEntity(self::OPERATION_SAVE);

        $primaryAlias = $this->getEntityAlias($primaryEntity[Schema::$entityKey]);

        $conditions = array();

        foreach ($params as $k => $value) {

            if (Utils::isUInt($k) && Utils::isUInt($value)) {
                $meta = $this->getEntityManager()->getClassMetadata($primaryEntity);

                $identifiers = $meta->getIdentifierFieldNames();

                if (!count($identifiers)) {
                    throw new Exception("Identifiers are empty for `$primaryEntity`");
                }

                if (count($identifiers) > 1) {
                    throw new Exception("More than one identifier for `$primaryEntity`. Use assoc case!");
                }

                $identifier = current($identifiers);

                $conditions[] = array(
                    $identifier => array(
                        'value'     => $value,
                        'operation' => \Doctrine\ORM\Query\Expr\Comparison::EQ)
                );
            } elseif (Utils::isUInt($k) && is_array($value)) {

                foreach ($value as $fieldName => $fValue) {

                    if (empty($value[$fieldName]['operation'])) {
                        $value[$fieldName]['operation'] = \Doctrine\ORM\Query\Expr\Comparison::EQ;
                    }
                }

                $conditions[] = $value;
            } elseif (is_string($k)) {

                $conditions[0][$k] = array('value' => $value);

                if (empty($conditions[0][$k]['operation'])) {
                    $conditions[0][$k]['operation'] = \Doctrine\ORM\Query\Expr\Comparison::EQ;
                }
            }
        }

        if (!count($conditions)) {
            return false;
        }

        $removedCountItems = 0;
        try {
            foreach ($conditions as $condition) {

                if ($this->isAutoTransactional()) {
                    $this->getEntityManager()->getConnection()->beginTransaction();
                }

                $qb = $this->getEntityManager()->createQueryBuilder()
                        ->delete($primaryEntity[Schema::$entityKey], $primaryAlias);

                foreach ($condition as $identifier => $value) {
                    $qb->andWhere(new \Doctrine\ORM\Query\Expr\Comparison("{$primaryAlias}.{$identifier}", $value['operation'], ":$identifier"))
                            ->setParameter($identifier, $value['value']);
                }

                $removedCountItems += $qb->getQuery()->execute();

                if ($this->isAutoTransactional()) {
                    $this->getEntityManager()->getConnection()->commit();
                }
            }
        } catch (\Exception $e) {
            if ($this->isAutoTransactional()) {
                $this->getEntityManager()->getConnection()->rollback();
            }
            throw new Exception('Deleting entity failed', null, $e);
        }

        return $removedCountItems;
    }

    /**
     * Reset the query builder.
     * If mark all flag and data stored as where and order will be deleted
     * false - Reset none | true - Reset all | 2 - Reset where | 3 - Reset order
     *
     * @param boolean | int $all
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     */
    public function resetQueryBuilder($all = false)
    {

        $this->queryBuilder = null;

        if (is_bool($all)) {
            $all = ($all == true) ? self::RESET_ALL : 9999;
        }

        switch ($all) {
            case self::RESET_ALL:
                $this->where = array();
                $this->order = array();
                break;
            case self::RESET_WHERE:
                $this->where = array();
                break;
            case self::RESET_ORDER:
                $this->order = array();
                break;
        }

        return $this;
    }

    /**
     * Get Object by ID
     * If data for selection is not matched return false
     * If data for selection is match, but no result return null
     * if data for selection is match and have result, result returned
     *
     * @param array $data
     * @param \Doctrine\ORM\AbstractQuery $hydrationMode
     * @return boolean|null|Object|array
     * @throws Exception
     */
    public function fetchOne($data, $hydrationMode = \Doctrine\ORM\AbstractQuery::HYDRATE_OBJECT)
    {

        $startEntity = $this->getStartEntity(self::OPERATION_READ);

        $primaryKeys = $this->getEntityManager()
                ->getClassMetadata($startEntity[Schema::$entityKey])
                ->getIdentifierFieldNames();

        $whereParams = array_intersect_key($data, array_flip($primaryKeys));

        if (empty($whereParams)) {
            return false;
        }

        foreach ($whereParams as $assoc => $param) {
            $this->addWhere($assoc, $param);
        }

        $result = $this->load($hydrationMode);

        $alias = $this->getAliasByEntityName($this->getStartEntity(self::OPERATION_READ));

        foreach ($whereParams as $assoc => $param) {

            foreach ($this->where as $keyWhere => $where) {
                if ($where['field'] == $alias . '.' . $assoc && $where['value'] == $param) {
                    unset($this->where[$keyWhere]);
                }
            }
        }

        if (count($result) > 0) {
            return current($result);
        }

        return null;
    }

    /**
     * Load all results from the query builder
     *
     * @param \Doctrine\ORM\AbstractQuery $hydrationMode
     * @return mixed
     * @throws Exception
     */
    public function load($hydrationMode = \Doctrine\ORM\AbstractQuery::HYDRATE_OBJECT)
    {
        $qb = $this->getQueryBuilder();

        try {
            $query     = $qb->getQuery();
            $query->setHydrationMode($hydrationMode);
            $paginator = new Paginator($query);
            $paginator->setUseOutputWalkers(false);
            $result    = \iterator_to_array($paginator->getIterator());
        } catch (\Exception $e) {
            throw new Exception('Failed loading prepared entity conditions from query builder', null, $e);
        }

        return $result;
    }

    /**
     * Returns count of all the results from the query builder
     *
     * @return integer
     * @throws Exception
     */
    public function getTotalCount()
    {
        $qb = $this->getQueryBuilder();

        try {
            $query     = $qb->getQuery();
            $test = $query->getSQL();
            $paginator = new Paginator($query);
            $paginator->setUseOutputWalkers(false);
            return $paginator->count();
        } catch (\Exception $e) {
            throw new Exception('Failed loading prepared entity conditions from query builder', null, $e);
        }
    }

    /**
     * Method used for debuging insted of the load() function.
     * It returns the DQL.
     *
     * @return string
     */
    public function getQueryDQL()
    {

        if (null === $this->queryBuilder) {
            $this->queryBuilder = $this->getEntityManager()
                    ->createQueryBuilder();
            $this->queryBuilder = $this->buildStatement($this->queryBuilder);
        }
        return $this->queryBuilder->getDQL();
    }

    /**
     * Sets the position of the first result to retrieve (the "offset").
     *
     * @param integer $firstResult The first result to return.
     *
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     */
    public function setFirstResult($firstResult)
    {
        $this->getQueryBuilder()->setFirstResult($firstResult);

        return $this;
    }

    /**
     * Sets the maximum number of results to retrieve (the "itemCountPerPage").
     *
     * @param integer $maxResults
     *
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     */
    public function setMaxResults($maxResults)
    {
        $this->getQueryBuilder()->setMaxResults($maxResults);

        return $this;
    }

    /**
     * Adds additional criteria to search for
     *
     * @param \Doctrine\Common\Collections\Criteria $criteria
     * @return \Common\Doctrine\Repository\RepositoryAbstract
     */
    public function addCriteria(Criteria $criteria)
    {
        $this->getQueryBuilder()->addCriteria($criteria);

        return $this;
    }
}
