<?php
namespace Docs\CommonBundle\Doctrine\Repository;

/**
 * Repository invalid argument exception
 */
class InvalidArgumentException extends \InvalidArgumentException
{
}
