<?php

namespace Docs\CommonBundle\Doctrine\Repository;

/**
 * General repository exception
 */
class Exception extends \Exception
{
}
