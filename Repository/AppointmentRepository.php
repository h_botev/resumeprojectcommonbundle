<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * AppointmentRepository
 */
class AppointmentRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [
            "note" => [
                "entity" => "\Docs\CommonBundle\Entity\Note",
                "required" => false
            ],
            "user" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true,
                "alias" => "CreatedBy"
            ],
            "withUser" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true
            ],
            "status" => [
                "entity" => "\Docs\CommonBundle\Entity\AppointmentStatus",
                "required" => true
            ]
        ],
        self::OPERATION_SAVE => []
    ];
}
