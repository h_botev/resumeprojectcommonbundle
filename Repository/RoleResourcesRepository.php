<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * RoleResourcesRepository
 */
class RoleResourcesRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [
            "resource" => [
                "entity" => "\Docs\CommonBundle\Entity\Resource",
                "required" => true
            ],
            "role" => [
                "entity" => "\Docs\CommonBundle\Entity\Role",
                "required" => true
            ]
        ],
        self::OPERATION_SAVE => []
    ];
}
