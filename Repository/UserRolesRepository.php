<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * UserRolesRepository
 */
class UserRolesRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [
            "user" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true
            ],
            "role" => [
                "entity" => "\Docs\CommonBundle\Entity\Role",
                "required" => true
            ]
        ],
        self::OPERATION_SAVE => []
    ];
}
