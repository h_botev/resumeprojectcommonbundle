<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

class UserRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [],
        self::OPERATION_SAVE => []
    ];

    /**
     * Get all currently active MD's
     * @return array
     */
    public function getAllActiveDocs()
    {
        $userRolesRepo = $this->getEntityManager()->getRepository('\Docs\CommonBundle\Entity\UserRoles');
        $queryBuilder = $userRolesRepo->createQueryBuilder('UserRoles');

        $queryBuilder->select("UserRoles, User")
                        ->join("UserRoles.user", "User")
                        ->join("UserRoles.role", "Role")
                        ->where($queryBuilder->expr()->eq("Role.name", ":role"))
                        ->andWhere($queryBuilder->expr()->eq("User.isActive", ":active"))
                        ->setParameter(":active", \Docs\CommonBundle\Entity\User::ACTIVE)
                        ->setParameter(":role", \Docs\CommonBundle\Entity\Role::ROLE_DOC);

        return $queryBuilder->getQuery()->getResult();
    }
}
