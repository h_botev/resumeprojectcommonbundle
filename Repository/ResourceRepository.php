<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * ResourceRepository
 */
class ResourceRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [],
        self::OPERATION_SAVE => []
    ];
}
