<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Entity\Reminder;
use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * ReminderRepository
 */
class ReminderRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [
            "note" => [
                "entity" => "\Docs\CommonBundle\Entity\Note",
                "required" => true
            ],
            "user" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true
            ],
            "createdBy" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true,
                "alias" => "CreatedBy"
            ]
        ],
        self::OPERATION_SAVE => []
    ];

    /**
     * Return the number of open reminders with scheduled date
     * in the past
     * @param int $userID
     * @return int
     */
    public function getCurrentlyOpenRemindersCount($userID)
    {
        $queryBuilder = $this->createQueryBuilder("Reminder");

        $queryBuilder->select("COUNT(Reminder.reminderID) as cnt")
                     ->andWhere($queryBuilder->expr()->eq("Reminder.createdBy", ":userID"))
                     ->andWhere($queryBuilder->expr()->eq("Reminder.status", ":status"))
                     ->andWhere($queryBuilder->expr()->lt("Reminder.scheduled", ":now"))
                     ->setParameter(":userID", $userID)
                     ->setParameter(":status", Reminder::STATUS_OPEN)
                     ->setParameter(":now", new \DateTime());


        return $queryBuilder->getQuery()->getOneOrNullResult()['cnt'];
    }
}
