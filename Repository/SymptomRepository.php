<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * SymptomRepository
 */
class SymptomRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [],
        self::OPERATION_SAVE => []
    ];
}
