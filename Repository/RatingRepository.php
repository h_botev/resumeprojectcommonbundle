<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * RatingRepository
 */
class RatingRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [
            "note" => [
                "entity" => "\Docs\CommonBundle\Entity\Note",
                "required" => true
            ],
            "user" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true
            ],
            "createdBy" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true,
                "alias" => "CreatedBy"
            ]
        ],
        self::OPERATION_SAVE => []
    ];
}
