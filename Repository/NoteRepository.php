<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * NoteRepository
 */
class NoteRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [
            "user" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true
            ]
        ],
        self::OPERATION_SAVE => []
    ];
}
