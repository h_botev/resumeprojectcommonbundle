<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * AppointmentSymptomsRepository
 */
class AppointmentSymptomsRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [
            "appointment" => [
                "entity" => "\Docs\CommonBundle\Entity\Appointment",
                "required" => true
            ],
            "symptom" => [
                "entity" => "\Docs\CommonBundle\Entity\Symptom",
                "required" => true
            ]
        ],
        self::OPERATION_SAVE => []
    ];
}
