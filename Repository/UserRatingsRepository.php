<?php
namespace Docs\CommonBundle\Repository;

use Docs\CommonBundle\Doctrine\Repository\AbstractRepository;

/**
 * UserRatingsRepository
 */
class UserRatingsRepository extends AbstractRepository
{
    protected $entitiesMap = [
        self::OPERATION_READ => [
            "user" => [
                "entity" => "\Docs\CommonBundle\Entity\User",
                "required" => true
            ],
            "rating" => [
                "entity" => "\Docs\CommonBundle\Entity\Rating",
                "required" => true
            ]
        ],
        self::OPERATION_SAVE => []
    ];
}
