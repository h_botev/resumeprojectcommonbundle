<?php
namespace Docs\CommonBundle\RestClient\Cache;

use Docs\RestClientBundle\Client\AbstractClient;
use Docs\CommonBundle\Cache\CacheableInterface;

/**
 * Abstract class that provides methods for caching and retrieving
 * items by ids from third party services
 * @author h.botev
 *
 */
abstract class CacheClient extends AbstractClient implements CacheableInterface
{
    use \Docs\CommonBundle\Cache\CacheableTrait;

    /**
     * Get single item from cache
     * @param mixed $id
     * @return array
     */
    public function getItemFromCache($id)
    {
        $item = $this->getCache()->fetch($this->getCacheKey($id));

        if ($item) {
            return $item;
        }

        $item = $this->find($id)
                        ->getData();

        $this->getCache()->save(
            $this->getCacheKey($id),
            $item,
            $this->getExpireTime()
        );

        return $item;
    }

    /**
     * Return the key for this id in the cache
     * @param mixed $id
     * @return string
     */
    protected function getCacheKey($id)
    {
        return static::CACHE_KEY . $id;
    }
}
