<?php
namespace Docs\CommonBundle\Cache;

use Doctrine\Common\Cache\CacheProvider;

/**
 * Interface that might be implemented by classes
 * that use cache
 * @author h.botev
 *
 */
interface CacheableInterface
{
    /**
     * Set the cache instance
     * @param \Doctrine\Common\Cache\CacheProvider $cache
     */
    public function setCache(CacheProvider $cache);

    /**
     * Return the cache instance
     * @return \Doctrine\Common\Cache\CacheProvider
     */
    public function getCache();

    public function setExpireTime($time);

    public function getExpireTime();
}
