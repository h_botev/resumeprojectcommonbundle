<?php
namespace Docs\CommonBundle\Cache;

use Doctrine\Common\Cache\CacheProvider;

/**
 * A trait that provides common functionallity for
 * using caches in a class
 * @author h.botev
 *
 */
trait CacheableTrait
{
    /**
     * @var \Doctrine\Common\Cache\CacheProvider
     */
    protected $cache;

    /**
     * @var int
     */
    protected $expireTime = 0;

    /**
     * Set the cache handler for the current object
     * @param CacheProvider $cache
     */
    public function setCache(CacheProvider $cache)
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * Get the current cache handler
     * @return \Doctrine\Common\Cache\CacheProvider
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * Set cache expiration time
     * @param int $time
     * @return \Docs\CommonBundle\Cache\CacheableTrait
     */
    public function setExpireTime($time)
    {
        $this->expireTime = $time;
        return $this;
    }

    /**
     * Return cache expiration time
     * @return int
     */
    public function getExpireTime()
    {
        return $this->expireTime;
    }
}
