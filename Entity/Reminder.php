<?php
namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reminder
 *
 * @ORM\Table(name="Reminders")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\ReminderRepository")
 * @UniqueEntity("reminderID")
 */
class Reminder extends AbstractEntity
{
    /**
     * Reminder open status
     * @var int
     */
    const STATUS_OPEN = 0;

    /**
     * Reminder closed status
     * @var int
     */
    const STATUS_CLOSED = 1;

    /**
     * @ORM\Column(name="reminderID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $reminderID;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userID", referencedColumnName="userID")
     * })
     */
    protected $user;

    /**
     * @var \Docs\CommonBundle\Entity\Note
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Note", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noteID", referencedColumnName="noteID")
     * })
     */
    protected $note;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="scheduled", type="datetime")
     */
    protected $scheduled;

    /**
     * @ORM\Column(name="status", type="smallint")
     */
    protected $status;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="createdBy", referencedColumnName="userID")
     * })
     */
    protected $createdBy;
}
