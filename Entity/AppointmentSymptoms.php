<?php
namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Docs\CommonBundle\Doctrine\AbstractEntity;

/**
 * AppointmentSymptoms
 *
 * @ORM\Table(name="AppointmentSymptoms")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\AppointmentSymptomsRepository")
 * @UniqueEntity("appointmentSymptomsID")
 */
class AppointmentSymptoms extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="appointmentSymptomsID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $appointmentSymptomsID;

    /**
     * @var \Docs\CommonBundle\Entity\Appointment
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Appointment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="appointmentID", referencedColumnName="appointmentID", nullable=false)
     * })
     */
    protected $appointment;

    /**
     * @var \Docs\CommonBundle\Entity\Symptom
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Symptom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="symptomID", referencedColumnName="symptomID", nullable=false)
     * })
     */
    protected $symptom;
}
