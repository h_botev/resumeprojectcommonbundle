<?php

namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;

/**
 * RoleResources
 *
 * @ORM\Table(name="RoleResources")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\RoleResourcesRepository")
 */
class RoleResources extends AbstractEntity
{
    /**
     *  @ORM\Id
     *  @ORM\Column(name="roleResourceID", type="integer")
     *  @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $roleResourceID;

    /**
     * @var \Docs\CommonBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Resource")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="resourceID", referencedColumnName="resourceID")
     * })
     */
    protected $resource;

    /**
     * @var \Docs\CommonBundle\Entity\Roles
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Role")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleID", referencedColumnName="roleID")
     * })
     */
    protected $role;

    /**
     * @ORM\Column(name="rights", type="integer")
     */
    protected $rights;
}
