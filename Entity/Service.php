<?php
namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Service
 *
 * @ORM\Table(name="Services", uniqueConstraints={@ORM\UniqueConstraint(name="serviceKey", columns={"serviceKey"})})
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\ServiceRepository")
 * @UniqueEntity("serviceKey")
 */
class Service extends AbstractEntity
{
    /**
     * @ORM\Column(name="serviceID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $serviceID;

    /**
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     *
     */
    protected $name;

    /**
     * @ORM\Column(name="serviceKey", type="string", length=32, nullable=false)
     */
    protected $serviceKey;
}
