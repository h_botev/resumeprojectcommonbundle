<?php

namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;

/**
 * UserRatings
 *
 * @ORM\Table(name="UserRatings")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\UserRatingsRepository")
 */
class UserRatings extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userRatingID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $userRatingID;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userID", referencedColumnName="userID")
     * })
     */
    protected $user;

    /**
     * @var \Docs\CommonBundle\Entity\Rating
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Rating")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ratingID", referencedColumnName="ratingID")
     * })
     */
    protected $rating;
}
