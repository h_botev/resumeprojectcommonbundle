<?php

namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;

/**
 * UserRoles
 *
 * @ORM\Table(name="UserRoles")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\UserRolesRepository")
 */
class UserRoles extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userRoleID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $userRoleID;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userID", referencedColumnName="userID")
     * })
     */
    protected $user;

    /**
     * @var \Docs\CommonBundle\Entity\Roles
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Role")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleID", referencedColumnName="roleID")
     * })
     */
    protected $role;
}
