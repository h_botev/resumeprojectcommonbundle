<?php

namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Docs\CommonBundle\Doctrine\AbstractEntity;

/**
 * Roles
 *
 * @ORM\Table(name="Roles")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\RoleRepository")
 */
class Role extends AbstractEntity
{
    const ROLE_GOOGLE_USER = "ROLE_GOOGLE_USER";

    /**
     * @var integer
     */
    const ROLE_DOC_ID = 5;

    const ROLE_DOC = "ROLE_DOC";

    /**
     * @ORM\Column(name="roleID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $roleID;

    /**
     * @Assert\Length(
     *      max=32,
     *      maxMessage = "The role name cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(name="name", type="string", length=32, nullable=true)
     */
    protected $name;
}
