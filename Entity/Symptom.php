<?php
namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Symptoms
 *
 * @ORM\Table(name="Symptoms")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\SymptomRepository")
 * @UniqueEntity("symptomID")
 */
class Symptom extends AbstractEntity
{
    /**
     * @ORM\Column(name="symptomID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $symptomID;

    /**
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    protected $name;
}
