<?php
namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Appointment
 *
 * @ORM\Table(name="Appointments")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\AppointmentRepository")
 * @UniqueEntity("appointmentID")
 */
class Appointment extends AbstractEntity
{
    const AVAILABILITY_THRESHOLD = 3;

    /**
     * Appointment open status
     * @var int
     */
    const STATUS_OPEN = 2;

    /**
     * @var string
     */
    const STATUS_OPEN_STRING = 'Open';

    /**
     * Appointment pending status
     * @var int
     */
    const STATUS_PENDING = 1;

    /**
     * @var string
     */
    const STATUS_PENDING_STRING = 'Pending';

    /**
     * Appointment closed status
     * @var int
     */
    const STATUS_CLOSED = 3;

    /**
     * @var string
     */
    const STATUS_CLOSED_STRING = 'Closed';

    /**
     * @ORM\Column(name="appointmentID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $appointmentID;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="scheduled", type="datetime")
     */
    protected $scheduled;

    /**
     * @var \Docs\CommonBundle\Entity\Note
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Note", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noteID", referencedColumnName="noteID")
     * })
     */
    protected $note;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userID", referencedColumnName="userID")
     * })
     */
    protected $user;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="withUserID", referencedColumnName="userID")
     * })
     */
    protected $withUser;

    /**
     * @var \Docs\CommonBundle\Entity\AppointmentStatus
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\AppointmentStatus", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusID", referencedColumnName="statusID")
     * })
     */
    protected $status;

    /**
     * @ORM\ManyToMany (targetEntity="\Docs\CommonBundle\Entity\Symptom")
     * @ORM\JoinTable (name="AppointmentSymptoms",
     *      joinColumns={@ORM\JoinColumn(name="appointmentID", referencedColumnName="appointmentID")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="symptomID", referencedColumnName="symptomID")}
     * )
     */
    protected $symptoms;
}
