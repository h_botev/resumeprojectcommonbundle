<?php

namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Docs\CommonBundle\Doctrine\AbstractEntity;

/**
 * Resource
 *
 * @ORM\Table(name="Resources")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\ResourceRepository")
 */
class Resource extends AbstractEntity
{
    /**
     * @ORM\Column(name="resourceID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $resourceID;

    /**
     * @Assert\Length(
     *      max=32,
     *      maxMessage = "The resource name cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(name="name", type="string", length=32, nullable=true)
     */
    protected $name;
}
