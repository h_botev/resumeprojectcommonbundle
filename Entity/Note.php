<?php
namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Notes
 *
 * @ORM\Table(name="Notes")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\NoteRepository")
 * @UniqueEntity("noteID")
 */
class Note extends AbstractEntity
{
    /**
     * @ORM\Column(name="noteID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $noteID;

    /**
     * @Assert\Length(
     *      max=1000,
     *      maxMessage = "The note cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(name="content", type="string", length=500, nullable=false)
     */
    protected $content;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userID", referencedColumnName="userID")
     * })
     */
    protected $user;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;
}
