<?php
namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AppointmentStatuses
 *
 * @ORM\Table(name="AppointmentStatuses")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\AppointmentStatusRepository")
 * @UniqueEntity("statusID")
 */
class AppointmentStatus extends AbstractEntity
{
    /**
     * @ORM\Column(name="statusID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $statusID;

    /**
     * @ORM\Column(name="name", type="string")
     */
    protected $name;
}
