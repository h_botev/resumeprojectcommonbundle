<?php

namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Docs\CommonBundle\Doctrine\AbstractEntity;

/**
 * Roles
 *
 * @ORM\Table(name="Ratings")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\RatingRepository")
 */
class Rating extends AbstractEntity
{
    /**
     * @ORM\Column(name="ratingID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $ratingID;

    /**
     * @var \Docs\CommonBundle\Entity\Note
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\Note", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noteID", referencedColumnName="noteID")
     * })
     */
    protected $note;

    /**
     * @ORM\Column(name="rating", type="integer", nullable=false)
     */
    protected $rating;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userID", referencedColumnName="userID")
     * })
     */
    protected $user;

    /**
     * @var \Docs\CommonBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Docs\CommonBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="createdBy", referencedColumnName="userID")
     * })
     */
    protected $createdBy;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;
}
