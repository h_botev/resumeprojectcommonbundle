<?php
namespace Docs\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Docs\CommonBundle\Doctrine\AbstractEntity;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\XmlRoot;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="Users")
 * @ORM\Entity(repositoryClass="Docs\CommonBundle\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 * @XmlRoot("user")
 */
class User extends AbstractEntity
{
    const ACTIVE = 1;

    /**
     *
     * @ORM\Column(name="userID", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $userID;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    protected $username;

    /**
     * @ORM\Column(name="firstName", type="string", length=45)
     * @Assert\NotBlank()
     */
    protected $firstName;

    /**
     * @ORM\Column(name="lastName", type="string", length=45)
     * @Assert\NotBlank()
     */
    protected $lastName;

    /**
     * @ORM\Column(name="password", type="string", length=64)
     * @Exclude
     */
    protected $password;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;

    /**
     * @ORM\Column(name="salt", type="string", length=64, unique=true)
     * @Exclude
     */
    protected $salt;

    /**
     * @ORM\Column(name="googleID")
     * @Exclude
     */
    protected $googleID;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @ORM\ManyToMany (targetEntity="\Docs\CommonBundle\Entity\Role")
     * @ORM\JoinTable (name="UserRoles",
     *      joinColumns={@ORM\JoinColumn(name="userID", referencedColumnName="userID")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="roleID", referencedColumnName="roleID")}
     *      )
     */
    protected $roles;

    public function hasSalt()
    {
        return $this->salt ? true : false;
    }

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->isActive = true;
    }
}
